package com.maonocodigo.projetospring.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maonocodigo.projetospring.model.Produto;
import com.maonocodigo.projetospring.repositories.ProdutoRepository;

@RestController
public class ProdutoController {

	@Autowired
	ProdutoRepository protoRepository;
	
	@GetMapping("/produtos")
	public ResponseEntity<List<Produto>> getAllProduto(){
		List<Produto> produtoList = protoRepository.findAll();
		if(produtoList.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}else
			return new ResponseEntity<List<Produto>>(produtoList, HttpStatus.OK);
	}
	
	@GetMapping("/produtos/{id}")
	public ResponseEntity<Produto> getOneProduto(@PathVariable(value="id")long id){
		Optional<Produto> produtoo = protoRepository.findById(id);
	if(! produtoo.isPresent()) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
		else {
			return new ResponseEntity<Produto>(produtoo.get(), HttpStatus.OK);
	}
	}
	
	@GetMapping("/")
	public ResponseEntity<Produto> findAll(){
		
		Produto produto1 = new Produto(1, "feijão", 5.90);
		return ResponseEntity.ok(produto1);
		
	}
	
}
