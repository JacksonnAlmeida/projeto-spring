package com.maonocodigo.projetospring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maonocodigo.projetospring.model.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

}
